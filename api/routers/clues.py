from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg

from .categories import CategoryOut
# Using routers for organization
# See https://fastapi.tiangolo.com/tutorial/bigger-applications/
router = APIRouter()


class ClueOut(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: CategoryOut
    canon: bool


class Clues(BaseModel):
    # page_count: int
    clues: list[ClueOut]


class Message(BaseModel):
    message: str


@router.get("/api/clues", response_model=Clues)
def clues_list(value: int, page: int = 0):
    # Uses the environment variables to connect
    # In development, see the docker-compose.yml file for
    #   the PG settings in the "environment" section
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT cl.id AS clue_id, cl.answer, 
                    cl.question, cl.value, cl.invalid_count,
                    ca.id AS category_id, ca.title,
                    ca.canon AS ca_canon, cl.canon AS cl_canon
                FROM clues cl
                JOIN categories ca ON cl.category_id = ca.id
                WHERE value = %s
                LIMIT 100 OFFSET %s
            """,
                [value, page * 100],
            )

            results = []
            for row in cur.fetchall():
                clue = {}
                clue_fields = [
                    "clue_id",
                    "answer",
                    "question",
                    "value",
                    "invalid_count",
                    "cl_canon",
                ]
                for i, column in enumerate(cur.description):
                        if column.name in clue_fields:
                            clue[column.name] = row[i]
                clue["id"] = clue["clue_id"]
                clue["canon"] = clue["cl_canon"]

                category = {}
                category_fields = [
                    "category_id",
                    "title",
                    "ca_canon",
                ]
                for i, column in enumerate(cur.description):
                        if column.name in category_fields:
                            category[column.name] = row[i]
                category["id"] = category["category_id"]
                category["canon"] = category["ca_canon"]

                clue["category"] = category

                results.append(clue)
            return Clues(clues=results)
            # return results ??


@router.get(
    "/api/clues/{clue_id}",
    response_model=ClueOut,
    responses={404: {"model": Message}},
)
def get_clue(clue_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT cl.id AS clue_id, cl.answer, 
                    cl.question, cl.value, cl.invalid_count,
                    ca.id AS category_id, ca.title,
                    ca.canon AS ca_canon, cl.canon AS cl_canon
                FROM clues cl
                JOIN categories ca ON cl.category_id = ca.id
                WHERE cl.id = %s
            """,
                [clue_id],
            )
            row = cur.fetchone()
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Clue not found"}
            
            clue = {}
            clue_fields = [
                "clue_id",
                "answer",
                "question",
                "value",
                "invalid_count",
                "cl_canon",
            ]
            for i, column in enumerate(cur.description):
                if column.name in clue_fields:
                    clue[column.name] = row[i]
            clue["id"] = clue["clue_id"]
            clue["canon"] = clue["cl_canon"]

            category = {}
            category_fields = [
                "category_id",
                "title",
                "ca_canon",
            ]
            for i, column in enumerate(cur.description):
                if column.name in category_fields:
                    category[column.name] = row[i]
            category["id"] = category["category_id"]
            category["canon"] = category["ca_canon"]

            clue["category"] = category
            return clue


@router.get(
    "/api/random-clue",
    response_model=ClueOut,
)
def get_random_clue(valid: bool = True):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            if valid:
                cur.execute(
                    """
                    SELECT cl.id AS clue_id, cl.answer, 
                        cl.question, cl.value, cl.invalid_count,
                        ca.id AS category_id, ca.title,
                        ca.canon AS ca_canon, cl.canon AS cl_canon
                    FROM clues cl
                    JOIN categories ca ON cl.category_id = ca.id
                    WHERE cl.invalid_count = 0
                    ORDER BY RANDOM() LIMIT 1;
                """,
                )
            else:
                cur.execute(
                    """
                    SELECT cl.id AS clue_id, cl.answer, 
                        cl.question, cl.value, cl.invalid_count,
                        ca.id AS category_id, ca.title,
                        ca.canon AS ca_canon, cl.canon AS cl_canon
                    FROM clues cl
                    JOIN categories ca ON cl.category_id = ca.id
                    ORDER BY RANDOM() LIMIT 1;
                """,
                )

            row = cur.fetchone()
            
            clue = {}
            clue_fields = [
                "clue_id",
                "answer",
                "question",
                "value",
                "invalid_count",
                "cl_canon",
            ]
            for i, column in enumerate(cur.description):
                if column.name in clue_fields:
                    clue[column.name] = row[i]
            clue["id"] = clue["clue_id"]
            clue["canon"] = clue["cl_canon"]

            category = {}
            category_fields = [
                "category_id",
                "title",
                "ca_canon",
            ]
            for i, column in enumerate(cur.description):
                if column.name in category_fields:
                    category[column.name] = row[i]
            category["id"] = category["category_id"]
            category["canon"] = category["ca_canon"]

            clue["category"] = category
            return clue

@router.delete(
    "/api/clues/{clue_id}",
    response_model=ClueOut,
    responses={400: {"model": Message}},
)
def update_clue(clue_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                UPDATE clues
                SET invalid_count = invalid_count + 1
                WHERE id = %s;
            """,
                [clue_id],
            )
    return get_clue(clue_id, response)

